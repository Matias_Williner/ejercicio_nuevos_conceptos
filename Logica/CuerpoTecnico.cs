﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class CuerpoTecnico:Persona
    {
        private bool _principal;
        public bool Principal { 
            get 
            {
                return _principal;
            }
            set 
            {
                _principal = value;
            } 
        }
        public int NumeroAyudante { get; set; }

        public override bool ProximoRetiro()
        {
            return false;
        }

        
    }
}
