﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Persona
    {
        public string Nombre { get; set; }
        public Nullable<int> Edad { get; set; }
        public string Nacionalidad { get; set; }

        ////////////////////////////////////////////// 4 ////////////////////////////////////////////////
        public List<string> ObtenerNombreNacionalidad ()
        {
            List<string> listaNombreNacionalidad = new List<string>();
            listaNombreNacionalidad.Add(Nombre);
            listaNombreNacionalidad.Add(Nacionalidad);
            return listaNombreNacionalidad;
        }

        ////////////////////////////////////////////// 5/6.2 ////////////////////////////////////////////////
        public virtual bool ProximoRetiro()
        {
            if (Edad.HasValue)
            {
                if (Edad.Value>=36)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                throw new Exception("Edad no tiene valor");
            }

            
        }

        public void Excepcion()
        {
            try
            {
                Edad = null;
            }
            catch (Exception e)
            {
                Edad=0;
                Console.WriteLine("Edad no tiene valor adecuado",e);
            }
            
        }

        

    }
}
