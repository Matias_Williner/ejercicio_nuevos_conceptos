﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public static class Extension
    {
        public static List<string> ObtenerListaFiltrada(this Partido partido)
        {
            List<string> listaFinal = new List<string>();
            List<string> listaPrevisional = partido.ObtenerDescripciónTotal();

            foreach (var item in listaPrevisional)
            {
                if (item.Contains("Gol"))
                {
                    listaFinal.Add(item);
                }
            }
            return listaFinal;
        }
    }
}
