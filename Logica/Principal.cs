﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public static class Principal
    {
        public static List<Partido> Partidos = new List<Partido>();

        public static string PartidoEnJuego { 
            get 
            {
                return PartidoEnJuego;
            }
            set 
            {
                PartidoEnJuego = value;
            }
        }

        /////////////////////////////////////////////// 3 //////////////////////////////////////////////////////////

        public static bool RegistrarPartidoDeterminado(List<Arbitro> arbitros, string ciudad, DateTime fechaHoraInicio, Equipo equipo1, Equipo equipo2,
            List<Gol> golesEquipo1, List<Gol> golesEquipo2, int tiempoJuego, List<Cambio> cambios, List<Tarjeta> tarjetasRojas, List<Tarjeta> tarjetasAmarillas)
        {
            if (arbitros != null && ciudad!="" && fechaHoraInicio!= default && equipo1!=null && equipo2!=null && golesEquipo1!=null && golesEquipo2 != null && tiempoJuego!=0 && 
                cambios!= null && tarjetasRojas!=null && tarjetasAmarillas!=null)
            {
                Partido nuevoPartido = new Partido();
                nuevoPartido.Arbitros = arbitros;
                nuevoPartido.FechaHoraInicio = fechaHoraInicio;
                nuevoPartido.CargarCiudad(ciudad);
                nuevoPartido.Equipo1 = equipo1;
                nuevoPartido.Equipo2 = equipo2;
                nuevoPartido.GolesEquipo1 = golesEquipo1;
                nuevoPartido.GolesEquipo2 = golesEquipo2;
                nuevoPartido.Cambios = cambios;
                nuevoPartido.TarjetasAmarillas = tarjetasAmarillas;
                nuevoPartido.TarjetasRojas = tarjetasRojas;
                nuevoPartido.Iniciar();
                Partidos.Add(nuevoPartido);
                return true;
            }
            return false;
        }

        ////////////////////////////////////////// 6 ////////////////////////////////////////////////////////////

        public static bool RegistrarPartidoAntesIniciar(List<Arbitro> arbitros, string ciudad, Equipo equipo1, Equipo equipo2)
        {
            if (arbitros != null && equipo1 != null && equipo2 != null && ciudad != "")
            {
                Partido nuevoPartido = new Partido();
                nuevoPartido.Arbitros = arbitros;
                nuevoPartido.CargarCiudad(ciudad);
                nuevoPartido.Equipo1 = equipo1;
                nuevoPartido.Equipo2 = equipo2;
                Partidos.Add(nuevoPartido);
                return true;
            }
            return false;

        }


        //////////////////////////////////Pruebas////////////////////////////////////////////

        /*public Persona BuscarPersonaSegunNombre(string nombre)
        {
            foreach (var persona in Personas)
            {
                if (nombre == persona.Nombre)
                {
                    return persona;
                }
            }
            return null;
        }

        public string nombre = "Hola";
        public string ObtenerNacionalidad(string nombre)
        {
            Persona persona = BuscarPersonaSegunNombre(nombre);
            persona.ObtenerNombreNacionalidad();
        }*/
           
        
    }
}
