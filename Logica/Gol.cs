﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public sealed class Gol:Incidencia
    {
        public Jugador Jugador { get; set; }
        
        public bool EnContra { get; set; }

        public string Arco { get; set; }

        
        public Gol(Jugador jugador, bool enContra, int minutoJuego, string arco)
        {
            Jugador = jugador;
            EnContra = enContra;
            MinutoJuego = minutoJuego;
            Arco = arco;
        }
        public override string ObtenerDescripcion()
        {
            if (EnContra==true)
            {
                return "Gol" + " - " + Jugador.Equipo + " - " + Jugador + " (EC) - " + MinutoJuego;
            }
            else
            {
                return "Gol" + " - " + Jugador.Equipo + " - " + Jugador + " - " + MinutoJuego;
                
            }
            
        }
    }
}
