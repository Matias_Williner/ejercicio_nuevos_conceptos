﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public abstract class Incidencia
    {
        public int MinutoJuego { get; set; }

        ////////////////////////////////////////////////// 9 ///////////////////////////////////////////////////
        public abstract string ObtenerDescripcion();
        
    }
}
