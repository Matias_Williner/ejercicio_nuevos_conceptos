﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Equipo
    {
        public string Nombre { get; set; }
        public int Puntos { get; set; }

        public List<Jugador> Jugadores { get; set; }

        public List<CuerpoTecnico> CuerposTecnicos { get; set; }


    }
}
