﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public sealed class Tarjeta:Incidencia
    {
        public Jugador JugadorRecibio { get; set; }

        public Colores Color { get; set; }

        public string TarjetaAsociada { get; set; }

        public Tarjeta(Jugador jugador, int minutoJuego, Colores color, string tarjetaAsociada )
        {
            JugadorRecibio = jugador;
            MinutoJuego = minutoJuego;
            Color = color;
            TarjetaAsociada = tarjetaAsociada;
        }

        public enum Colores
        {
            Amarilla=1,
            Roja=2,
        }

        public override string ObtenerDescripcion()
        {
            if (TarjetaAsociada=="Rojo")
            {
                return Color + " + " + TarjetaAsociada + " - " + JugadorRecibio + " - " + MinutoJuego;
            }
            else
            {
                return Color + " - " + JugadorRecibio + " - " + MinutoJuego;
            }
            
        }
    }
}
