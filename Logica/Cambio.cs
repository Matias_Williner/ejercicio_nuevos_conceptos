﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public sealed class Cambio:Incidencia
    {
        public Jugador JugadorEntra { get; set; }

        public Jugador JugadorSale { get; set; }

        public override string ObtenerDescripcion()
        {
            return "Cambio" + " - " + JugadorEntra + " " + "x" + " " + JugadorSale + " - " + MinutoJuego;
        }
    }
}
